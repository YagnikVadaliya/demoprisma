const {
  responseWithError,
  responseWithData,
} = require("../comman-fn/commanFunction");
const { statusCodes } = require("../constant/statuscode");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const register = async (req, res) => {
  try {
    const client = req.db;
    const { first_name, last_name, email, password } = req.body;

    let userExist = await client.users.findFirst({
      where: { email },
    });

    if (userExist) {
      return responseWithError(
        res,
        statusCodes.bad_request_code,
        null,
        "User already registered."
      );
    }

    let salt = await bcrypt.genSalt(10);

    let hashedPassword = await bcrypt.hash(password, salt);

    let user = await client.users.create({
      data: {
        first_name,
        last_name,
        email,
        password: hashedPassword,
      },
    });

    let token = jwt.sign(
      { userId: user.user_id },
      process.env.TOKEN_ENCRYPTION_KEY
    );

    return responseWithData(
      res,
      statusCodes.success_code,
      { access_token: token },
      "User registered successfully"
    );
  } catch (error) {
    return responseWithError(
      res,
      statusCodes.server_error_code,
      error,
      error.message
    );
  }
};

const login = async (req, res) => {
  try {
    const client = req.db;
    const { email, password } = req.body;

    let userExist = await client.users.findFirst({
      where: { email },
    });

    if (!userExist) {
      return responseWithError(
        res,
        statusCodes.bad_request_code,
        null,
        "User Does not Exist."
      );
    }

    let hashedPassword = await bcrypt.compare(password, userExist.password);

    if (!hashedPassword) {
      return responseWithError(
        res,
        statusCodes.bad_request_code,
        null,
        "Invalid password."
      );
    }

    let token = jwt.sign(
      { userId: userExist.user_id },
      process.env.TOKEN_ENCRYPTION_KEY
    );

    return responseWithData(
      res,
      statusCodes.success_code,
      { ...userExist, access_token: token },
      "User Loggged in successfully"
    );
  } catch (error) {
    return responseWithError(
      res,
      statusCodes.server_error_code,
      error,
      error.message
    );
  }
};
const getUser = async (req, res) => {
  try {
    return responseWithData(
      res,
      statusCodes.success_code,
      req.user,
      "User Data fetched successfully"
    );
  } catch (error) {
    return responseWithError(
      res,
      statusCodes.server_error_code,
      error,
      error.message
    );
  }
};

module.exports = {
  register,
  login,
  getUser,
};
