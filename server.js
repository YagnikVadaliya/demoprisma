require("dotenv").config();
const bodyParser = require("body-parser");
const express = require("express");
const cors = require("cors");
const app = express();

const port = process.env.PORT || 3000;

app.use(express.json());
// app.use(express.urlencoded())

app.use(cors());

app.use(require("./middlewares/db"));
app.use("/", require("./router"));

app.get("/", (req, res) => {
  res.send("Welcome to backend");
});

app.use(function (req, res) {
  return res.status(404).send({ status: 404, message: "URL not found" });
});

app.listen(port, () => {
  console.log(`server running on http://localhost:${port}`);
});
