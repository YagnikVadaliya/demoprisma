const Joi = require("joi");
const { responseWithError } = require("../comman-fn/commanFunction");
const { statusCodes } = require("../constant/statuscode");
const authBR = require("../business-logic/auth");

const register = async (req, res) => {
  try {
    const dataFormat = Joi.object({
      first_name: Joi.string().required(),
      last_name: Joi.string().required(),
      email: Joi.string().email().required(),
      password: Joi.string().required().min(5).max(20),
    });

    const { error } = dataFormat.validate(req.body);

    if (error) {
      return responseWithError(
        res,
        statusCodes.bad_request_code,
        error.message,
        "Invalid Input"
      );
    }

    return authBR.register(req, res);
  } catch (error) {
    return responseWithError(
      res,
      statusCodes.server_error_code,
      error.message,
      error.message
    );
  }
};

const login = async (req, res) => {
  try {
    const dataFormat = Joi.object({
      email: Joi.string().email().required(),
      password: Joi.string().required().min(5).max(20),
    });

    const { error } = dataFormat.validate(req.body);

    if (error) {
      return responseWithError(
        res,
        statusCodes.bad_request_code,
        error,
        "Invalid Input"
      );
    }

    return authBR.login(req, res);
  } catch (error) {
    return responseWithError(
      res,
      statusCodes.server_error_code,
      error.message,
      error.message
    );
  }
};

const getUser = async (req, res) => {
  try {
    return authBR.getUser(req, res);
  } catch (error) {
    return responseWithError(
      res,
      statusCodes.server_error_code,
      error.message,
      error.message
    );
  }
};

module.exports = {
  register,
  login,
  getUser,
};
