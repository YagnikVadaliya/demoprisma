const router = require("express").Router();
const userController = require("./auth");

router.use("/user", userController);

module.exports = router;
