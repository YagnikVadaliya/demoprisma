const router = require("express").Router();
const authController = require("../controller/auth");
const checkToken = require("../middlewares/checkToken");

router.post("/register", authController.register);

router.post("/login", authController.login);

router.get("/get-user", checkToken, authController.getUser);

module.exports = router;
