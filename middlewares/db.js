const { PrismaClient } = require("@prisma/client");
const { responseWithError } = require("../comman-fn/commanFunction");
const { statusCodes } = require("../constant/statuscode");

const appendDB = async (req, res, next) => {
  let prisma = new PrismaClient();
  try {
    req.db = prisma;
    next();
  } catch (error) {
    return responseWithError(
      res,
      statusCodes.server_error_code,
      error,
      error.message
    );
  } finally {
    prisma.$disconnect();
  }
};

module.exports = appendDB;
