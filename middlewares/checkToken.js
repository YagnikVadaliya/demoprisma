const { responseWithError } = require("../comman-fn/commanFunction");
const { statusCodes } = require("../constant/statuscode");
const jwt = require("jsonwebtoken");

const checkToken = async (req, res, next) => {
  try {
    let access_token = req.headers["authorization"];

    access_token = access_token.split(" ")[1];

    await jwt.verify(
      access_token,
      process.env.TOKEN_ENCRYPTION_KEY,
      async function (err, decoded) {
        if (err) {
          return responseWithError(
            res,
            statusCodes.unauthorized_code,
            err,
            err.message
          );
        }

        const userData = await req.db.users.findFirst({
          where: {
            user_id: decoded.userId,
          },
          select: {
            user_id: true,
            first_name: true,
            last_name: true,
            email: true,
          },
        });

        req.user = userData;
      }
    );

    console.log("req.user :>> ", req.user);
    next();
  } catch (error) {
    return responseWithError(
      res,
      statusCodes.server_error_code,
      error,
      error.message
    );
  }
};

module.exports = checkToken;
