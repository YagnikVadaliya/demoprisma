module.exports = {
  responseWithData: (res, statusCode = 200, data = {}, message = "") => {
    return res.status(statusCode).send({
      data,
      error: null,
      message,
    });
  },
  responseWithError: (res, statusCode = 500, error = [], message = "") => {
    return res.status(statusCode).send({
      data: null,
      error,
      message,
    });
  },
};
